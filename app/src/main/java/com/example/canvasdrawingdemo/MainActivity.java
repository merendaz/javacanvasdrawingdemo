package com.example.canvasdrawingdemo;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // setup our system for drawing (get your tools ready!)
        // ------------------

        // 1. setup the frame
        ImageView imageView = findViewById(R.id.imageView);
        Bitmap b = Bitmap.createBitmap(300, 500, Bitmap.Config.ARGB_8888);

        // 2. setup the canvas
        Canvas canvas = new Canvas(b);

        // 3. setup your paintbrush
        Paint paintbrush = new Paint();


        // draw some stuff on the canvas
        // ------------------


        // --------------------
        // DRAW SOME LINES
        // --------------------

        // 1. Set the background color to black
        canvas.drawColor(Color.BLACK);

        // 2. Choose a YELLOW crayon!
        paintbrush.setColor(Color.YELLOW);

        // 3. draw a straight line (yellow)
        canvas.drawLine(10, 50, 200, 50, paintbrush);

        // 4. Change to a GREEN crayon!
        paintbrush.setColor(Color.GREEN);

        // 5. draw a diagonal line (red)
        canvas.drawLine(10, 50, 200, 150, paintbrush);


        // --------------------
        // DRAW SOME SQUARES
        // --------------------

        // 1. Change crayon color to white
        paintbrush.setColor(Color.WHITE);

        // 2. Draw a 20x20 square
        canvas.drawRect(100, 100, 120, 120, paintbrush);

        // 3. Draw a 50x50 square
        canvas.drawRect(150, 150, 200, 200, paintbrush);

        // 4. Draw a circle (Filled)
        canvas.drawCircle(220, 100,40, paintbrush);

        // 5. Draw a circle (Stroke)
        paintbrush.setStyle(Paint.Style.STROKE);
        // Defining the Width of the Stroke
        paintbrush.setStrokeWidth(2);  //2 pixel line width
        paintbrush.setColor(Color.GRAY);
        canvas.drawCircle(120, 200,20, paintbrush);

        // 6. Drawing a Filled Circle
        paintbrush.setStyle(Paint.Style.FILL);
        paintbrush.setColor(Color.LTGRAY);
        canvas.drawCircle(120, 200,18, paintbrush);

        // 7. Using the Circle to Draw a Stick Person
        // 7a. draw a straight vertical line
        canvas.drawLine(120, 220, 120, 300, paintbrush);
        // 7b. draw a straight horizontal line
        canvas.drawLine(70, 250, 170, 250, paintbrush);
        // 7c. draw a diagonal line (left leg)
        canvas.drawLine(120, 300, 90, 350, paintbrush);
        // 7d. draw a diagonal line (right leg)
        canvas.drawLine(120, 300, 150, 350, paintbrush);

        // --------------------
        // DRAW SOME SQUARES - TEST
        // --------------------

        // 1. Set the text size
        paintbrush.setTextSize(40);
        paintbrush.setStyle(Paint.Style.FILL);

        // 2. Draw text onto screen
        canvas.drawText("HELLO WORLD", 10, 400, paintbrush);

        // 3. Draw some more text
        paintbrush.setTextSize(18);
        paintbrush.setColor(Color.YELLOW);
        canvas.drawText("GOODBYE WORLD!", 10, 450, paintbrush);


        // put the canvas into the frame
        // ------------------
        imageView.setImageBitmap(b);


    }
}
